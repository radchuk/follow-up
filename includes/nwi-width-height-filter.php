<?php
	function scripts_nwi_slider_hw(){
		// Register the script first.
		wp_register_script( 'nwi_slider_hw', site_url().'/wp-content/themes/canvaschild/includes/js/nwi-price-slider.js' );

		// Now we can localize the script with our data.
		$ajaxurl = admin_url('admin-ajax.php');
		wp_localize_script( 'nwi_slider_hw', 'ajax_url', $ajaxurl );

		// The script can be enqueued now or later.
		wp_enqueue_script( 'nwi_slider_hw' );

		//wp_enqueue_script( 'nwi_slider_hw' , site_url().'/wp-content/themes/canvaschild/includes/js/nwi-price-slider.js', 'jquery');
	}
	add_action( 'wp_enqueue_scripts', 'scripts_nwi_slider_hw' );

	// Creating the widget
	class wpb_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'nwi_filter_hw_widget',
			__('NWI Filter HW Widget', 'wpb_widget_domain'),
			array( 'description' => __( 'Filter Height Width', 'wpb_widget_domain' ), )
			);
	}
	 
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
				global $_chosen_attributes, $wpdb, $wp;

				extract( $args );

				if ( ! is_post_type_archive( 'product' ) && ! is_tax( get_object_taxonomies( 'product' ) ) )
					return;

				if ( sizeof( WC()->query->unfiltered_product_ids ) == 0 )
					return; // None shown - return

				$min_height = isset( $_GET['min_height'] ) ? esc_attr( $_GET['min_height'] ) : '';
				$max_height = isset( $_GET['max_height'] ) ? esc_attr( $_GET['max_height'] ) : '';
				$min_width = isset( $_GET['min_width'] ) ? esc_attr( $_GET['min_width'] ) : '';
				$max_width = isset( $_GET['max_width'] ) ? esc_attr( $_GET['max_width'] ) : '';

				//wp_enqueue_script( 'nwi_hw_filter-slider' );

				echo $before_widget . $before_title . $after_title;


				echo '<form method="get" action="">
					<div class="height_slider_wrapper">
						<div class="height_slider" style="display:none;"></div>
						<div class="height_slider_amount">
							<input type="text" id="min_height" name="min_height" value="' . esc_attr( $min_height ).'" placeholder="'.__('Min height', 'woocommerce' ).'" />
							<input type="text" id="max_height" name="max_height" value="' . esc_attr( $max_height ).'" placeholder="'.__( 'Max height', 'woocommerce' ).'" />
							<button type="submit" class="button">'.__( 'Filter', 'woocommerce' ).'</button>
							<div class="height_label" style="display:none;">
								'.__( 'Height:', 'woocommerce' ).' <span class="from"></span> &mdash; <span class="to"></span>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="width_slider_wrapper">
						<div class="width_slider" style="display:none;"></div>
						<div class="width_slider_amount">
							<input type="text" id="min_width" name="min_width" value="' . esc_attr( $min_width ) .'" placeholder="'.__('Min width', 'woocommerce' ).'" />
							<input type="text" id="max_width" name="max_width" value="' . esc_attr( $max_width ) .'" placeholder="'.__( 'Max width', 'woocommerce' ).'" />
							<button type="submit" class="button">'.__( 'Filter', 'woocommerce' ).'</button>
							<div class="height_label" style="display:none;">
								'.__( 'Width:', 'woocommerce' ).' <span class="from"></span> &mdash; <span class="to"></span>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</form>';

				echo $after_widget;
			}
		
		         
		// Widget Backend
		public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
		}
		else {
		$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
		}
		     
		// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
		}
	} // Class wpb_widget ends here
	 
	// Register and load the widget
	function wpb_load_widget() {
	    register_widget( 'wpb_widget' );
	}
	add_action( 'widgets_init', 'wpb_load_widget' );


	add_action( 'wp_ajax_nwi_return_wp_query_hw', 'nwi_return_wp_query_hw' );
	add_action( 'wp_ajax_nopriv_nwi_return_wp_query_hw', 'nwi_return_wp_query_hw' );
		function nwi_return_wp_query_hw() {
				

			$args = array(		
					'post_per_page' => '10'
				);
			
			$query = new WP_Query( $args );
			$to_sent = '';	
				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						 $query->the_post();
						 $to_sent .= get_the_title();
					}

				} else {
					$to_sent .= 'no posts found';
				}


			echo json_encode($to_sent);
			exit();
		}