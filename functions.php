<?php

/*
Add any custom functions to your child theme here
*/
add_action( 'wp_enqueue_scripts', 'nwi_enqueue_stuff' );
	function nwi_enqueue_stuff() {	
			wp_enqueue_script( 'print',     get_stylesheet_directory_uri()  . '/js/print.js', array('jquery'),  false , true);	
	    }

/** Remove reviews tab */
add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {

 unset($tabs['reviews']);

 return $tabs;
}

/*
 * Removes products count after categories name 
 */
add_filter( 'woocommerce_subcategory_count_html', 'woo_remove_category_products_count' );
 
function woo_remove_category_products_count() {
  return;
}

include_once dirname(__FILE__).'/includes/nwi-width-height-filter.php';

function print_button( $style = false ){

    $html =  '<a href="#" class="btn">
                <i class="print"></i>
                Print
            </a>' ;

    if ( $style )
        $html = '';

    return $html;

}
    
?>