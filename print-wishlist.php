<?php get_header();
$type=is_a_recipe($post);
$notes = get_field('notes');
?>
<div id="main">		
			<div id="content">

<div 
     style="clear: both;" 
     itemscope itemtype="http://schema.org/Recipe" 
     class="nmtg-recipe-item-wrapper"
     >


    <?php //$user = get_userdata($recipe_post->post_author); ?>
    <meta itemprop="author" content="<?php //echo $user->data->display_name; ?>">
    <meta itemprop="datePublished" content="<?php //echo $recipe_post->post_date; ?>">



<?php if ( have_posts() ): ?>
    <div class="row row-clone">
       <div class="col-md-12 mb20">
                    <h2>
                        <?php the_title(); ?>
                    </h2>
        </div>
 <?php if (has_post_thumbnail( $post->ID ) ): ?>	
        <div class="col-md-6">
        <?php the_post_thumbnail('weekly-thumbnail'); ?>
        </div>   
<?php endif; ?>
 <?php
		$easy_recipe = $post->post_content;
      $str=strpos($easy_recipe, "<div");
      $content_easy=substr($easy_recipe, 0, $str); 
if(!empty($content_easy)) { ?>
       <p itemprop="description">			
           <?php echo $content_easy; ?>
      </p>
<?php } else { ?>
		 <p itemprop="description">
				<?php echo $post->post_content; ?>
       </p>
<?php } ?>
<?php if ( $notes) : ?>
         <div class="col-md-6">
<?php foreach($notes as $note) {
			foreach($note as $key => $not) { ?>					       
             <div class="recipe_divider"></div>
                  <h4 class="recipe-title">Notes</h4>
             <div class="old-recipe-notes">
                   <?php echo $not; ?>
              </div>
<?php
	}
}?>
         </div>
<?php endif; ?>
		<div class="row ">
				<div class="col-md-12">
					  <div class="col-md-6">
						    <?php if( is_user_logged_in() ) : ?>
						      <div class="col-md-3">
						          <?php wpfp_link(); ?>
						      </div>
						      <div class="col-md-3">
						           <?php NMTG_link(); ?>
						      </div>
						      <?php endif; ?>
						      <div class="col-md-3">
						           <?php $print_button = new WPURP_Template_Recipe_Print_Button();
						echo html_display_print_button();?>
						      </div> 
						       <div class="col-md-3">                  
							         <?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings(); endif; ?>   
						      </div>    
					   </div>
				</div>
		  </div>		
   </div>
</div>
	 	<?php endif;  ?>
<?php
foreach($type as $massiv)
{
/*title for easyrecipe and acf template*/							
if(isset($massiv['title_span'][0])){
			$title = $massiv['title_span'][0];			
}elseif(isset($massiv['title_div'][0])){
			$title =	$massiv['title_div'][0];				
/*title for WUR*/									
} elseif(isset($massiv['title'])) {
	$title = $massiv['title'];							
}
/*servings for easy recipe and acf*/	
if(isset($massiv["erhead"]["Serves"])) {
	$description_servings = $massiv["erhead"]["Serves"];							
}
if(isset($massiv["erhead"]["Prep time"])){
	$description_servings .= $massiv["erhead"]["Prep time"];			
}
if(isset($massiv["erhead"]["Cook time"])){
	$description_servings .=	$massiv["erhead"]["Cook time"];												
}
if(isset($massiv["erhead"]["Total time"])) {
	$description_servings .= $massiv["erhead"]["Total time"];							
}
if(isset($massiv["erhead"]["Passive time"])) {
	$description_servings .= $massiv["erhead"]["Passive time"];							
}

/*servings for WUR*/	
if(isset($massiv[0]["servings"])){
$servings_wur = $massiv[0]["servings"];
}
if(isset($massiv[0]["cook time"])){
$cookTime_wur = $massiv[0]["cook time"];
}
if(isset($massiv[0]["prep time"])){
$prep_time_wur = $massiv[0]["prep time"];
}
if(isset($massiv[0]["passive time"])){
$passive_time_wur = $massiv[0]["passive time"];
}
/*Nutritional information for acf*/	
if(isset($massiv["nutrition_table"][0])){
	$recipe_nutritional = $massiv["nutrition_table"][0];							
}
/*ingridients and instructions for all recipe template*/	
if(isset($massiv["ingridients"][0])){
$ingredients = $massiv["ingridients"][0];								
}

if(isset($massiv["instructions"][0])){
$instr = $massiv["instructions"][0];							
}
/*Nutritional information for WUR*/
if(isset($massiv['notes'])){
$recipe_notes_wur = $massiv['notes'];								
}
$notes_wur = '';
?>	 
            

 <div class="row row-clone">
       <?php if($title){?>
		<div class="col-md-12">           
          <div class="recipe_divider"></div> 
             <h3>
				<?php echo $title;?>
				</h3>
		</div>
<?php } ?>
		<div class="col-md-12 col-serving">
			 <div class="col-md-8">
					<div class="col-md-4">
							<span class="recipe-information-servings">
								<?php
								/*servings easy recipe and ACF recipe*/
								if(!empty($description_servings)){
								echo $description_servings;
								}
								/*WUR servings recipe*/		  
								if(!empty ($servings_wur)){
								if(trim(strip_tags($servings_wur)) != ''){?>
								<span><?php _e( 'Servings', 'nomoretogo' ); ?> :</span>
								<?php echo $servings_wur;
								   }
								}
								if(!empty ($cookTime_wur)){
								if(trim(strip_tags($cookTime_wur)) != ''){?>
								<span><?php _e('Cook Time', 'nomoretogo' ); ?> :</span>
								 <?php echo $cookTime_wur;
									}
								}
								if(!empty ($prep_time_wur)){
								if(trim(strip_tags($prep_time_wur)) != ''){?>
								<span><?php _e('Prep Time', 'nomoretogo' ); ?> : </span>
								<?php echo $prep_time_wur;
									}
								}
								if(!empty ($passive_time_wur)){
								if(trim(strip_tags($passive_time_wur)) != ''){?>
								<span><?php _e('Passive Time', 'nomoretogo' ); ?> : </span>
								<?php echo $passive_time_wur;
									}
								}?>
							</span>		
					 </div>
			 </div>
	 </div>
</div>    
<div class="col-md-6">
<?php
/*all template Ingredients */		
if( !empty($ingredients) ) { ?>
     <div class="ERIngredientsHeader">
          <?php _e('Ingredients', 'nomoretogo' ); ?>
     </div>
     <ul class="ingredients" data-servings="">
          <?php echo $ingredients;?>
     </ul>
	  <hr / class="hidden-xs">
<?php } ?>

<?php
/*all template Instructions */	
if( !empty($instr) ) { ?>
<div class="ERInstructionsHeader">
     <?php _e( 'Instructions', 'nomoretogo' ); ?>
</div>
<ol class="recipe-instructions">
    <?php echo $instr;?>
</ol>
<hr / class="hidden-xs">
<?php } ?>					 
<?php
/*nutrition information for WUR*/
if(isset($massiv['notes'][0])){?>

  <div class="row row-clone hidden-xs">
       <div class="ERInstructionsHeader">
       <?php _e( 'Nutritional Information', 'nomoretogo' ); ?>
       </div>
       <div class="col-md-12">
            <div class="recipe-notes">          
            <?php foreach($massiv['notes'][0] as $notes_wur )
				{
						echo 	$notes_wur.'<br>';						
				}?>

				</div>
       </div>
  </div>
<?php } ?>
 <?php
 /*nutrition information for ACF*/
 if( !empty( $recipe_nutritional) ) : ?>
 <hr / class="hidden-xs">
<div class="row row-clone hidden-xs">
	 <div class="ERInstructionsHeader">
			<?php _e( 'Nutritional Information', 'nomoretogo' ); ?>
	  </div>
		<div class="col-md-12">
			  <div class="recipe-notes">
					 <table border="1" width="800" cellspacing="0" cellpadding="0">
							<?php echo $recipe_nutritional;?>
					 </table>  
				</div>
		  </div>
 </div>
 <?php endif; ?>
 <?php
 }
?>
<hr / class="hidden-xs">
<?php comments_template(); ?>
						</div>
				</div>                                                     
		</div>
</div>
<?php get_footer(); ?>