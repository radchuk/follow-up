<?php
/**
 * Variable product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $post;
		wp_enqueue_script( 'radio-onsingle-product', site_url().'/wp-content/themes/canvaschild/includes/js/variable-radio-button.js', 'jquery');								
?>

<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo $post->ID; ?>" data-product_variations="<?php echo esc_attr( json_encode( $available_variations ) ) ?>">
	<?php if ( ! empty( $available_variations ) ) : ?>
		<input type="hidden"  name="attribute_pa_option" value="" />
		<table class="variations" cellspacing="0">
			<thead>
				<tr>
					<th></th>
					<th class="left">Description</th>
					<th class="center">Dimensions</th>
					<th class="center">Price</th>
				</tr>
			<thead>
			<tbody>
				<?php
				$adding_to_cart = get_product(201);
				$attributes = $adding_to_cart->get_attributes();
				$variation = get_product(211);
				//var_dump($attributes );

				//var_dump($variation );
				foreach ($attributes as $attribute) {
					if (!$attribute['is_variation'])
						var_dump('!variation');
					//$value = sanitize_title(trim(stripslashes(
				}
				$iter = 0;
				?>
				<?php foreach ( $available_variations as $name => $options ) :  ?>
					<?php
							$iter++ ;
							$array = $options["attributes"];
							reset($array);
							$first_key = key($array);
						  	$nwi_attr = $array[$first_key]; 
					?>
					<tr>
						<td class="value" id='attribute-rad'>
						  	<input type="radio" name='attri' id='radio<?php echo $iter; ?>' data-nwiattr='<?php echo $nwi_attr;?>' value="<?php echo $options['variation_id']?>">
						  	<label  class='circle' for='radio<?php echo $iter; ?>'></label>
						</td> 
						<td class="value left" >
							<?php echo $nwi_attr;?>
						</td>
						<td class="value center">
						  	 <?php echo  $options["dimensions"]; ?>
						</td>
						<td class="value center"> 
						  	 <?php echo  $options["price_html"]; ?>
						</td>
					</tr>
		        <?php endforeach;?>
			</tbody>
		</table>

		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<div class="single_variation_wrap" style="display:none;">
			<?php do_action( 'woocommerce_before_single_variation' ); ?>

			<div class="single_variation"></div>

			<div class="variations_button">
				<?php woocommerce_quantity_input(); ?>
				<button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button>
			</div>

			<input type="hidden" name="add-to-cart" value="<?php echo $product->id; ?>" />
			<input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
			<input type="hidden" name="variation_id" value="" />
			

			<?php do_action( 'woocommerce_after_single_variation' ); ?>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<?php else : ?>

		<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>

	<?php endif; ?>

</form>

<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
